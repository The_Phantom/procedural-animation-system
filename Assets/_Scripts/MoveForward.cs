using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{

    [SerializeField]
    private Vector3 MotionVector;


    public void Update() {
        this.transform.position += MotionVector * Time.deltaTime;
    }
}
