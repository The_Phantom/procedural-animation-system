using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CurveCreator
{
	public static void CreateCurve(BezierSpline spline, List<Vector3> mainPoints, BezierControlPointMode mode, float controlPointResolution)
	{
        spline.Reset();
        controlPointResolution *= 0.5f;
		spline.controlPointHalfResolution = controlPointResolution;
		spline.SetControlPointMode(0, mode);
		spline.SetControlPointMode(3, mode);
		spline.points[0] = mainPoints[0];
		spline.points[1] = mainPoints[0] + (mainPoints[1] - mainPoints[0]).normalized * controlPointResolution;
		spline.points[2] = mainPoints[1] + (mainPoints[0] - mainPoints[1]).normalized * controlPointResolution;
		spline.points[3] = mainPoints[1];
		for (int i = 2; i < mainPoints.Count; i++)
		{
			spline.AddCurve(mainPoints[i]);
		}
		for (int i = 0; i < spline.points.Length; i++)
		{
			spline.EnforceMode(i);
		}
	}

}
