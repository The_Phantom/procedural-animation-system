using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierSpline: MonoBehaviour
{
	public Vector3[] points;
	public float controlPointHalfResolution;
    public bool showDebugLine = false;

	[SerializeField] private BezierControlPointMode[] modes;

	public int ControlPointCount
	{
		get
		{
			return points.Length;
		}
	}

	public int CurveCount
	{
		get
		{
			return (points.Length - 1) / 3;
		}
	}

	public Vector3 GetControlPoint(int index)
	{
		return points[index];
	}

	public void SetControlPoint(int index, Vector3 point)
	{
		if (index % 3 == 0)
		{
			Vector3 delta = point - points[index];
			if (index > 0)
			{
				points[index - 1] += delta;
			}
			if (index + 1 < points.Length)
			{
				points[index + 1] += delta;
			}
		}
		points[index] = point;
		EnforceMode(index);
	}

	

	public Vector3 GetPoint(float t)
	{
		int i;
		if (t >= 1f)
		{
			t = 1f;
			i = points.Length - 4;
		}
		else
		{
			t = Mathf.Clamp01(t) * CurveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}
		return Bezier.GetPoint(points[i], points[i + 1], points[i + 2], points[i + 3], t);
	}

	public Vector3 GetVelocity(float t)
	{
		int i;
		if (t >= 1f)
		{
			t = 1f;
			i = points.Length - 4;
		}
		else
		{
			t = Mathf.Clamp01(t) * CurveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}
		return Bezier.GetFirstDerivative(points[i], points[i + 1], points[i + 2], points[i + 3], t);
	}

	public Vector3 GetDirection(float t)
	{
		return GetVelocity(t).normalized;
	}
	public void Reset()
	{
		points = new Vector3[] {
			new Vector3(1f, 0f, 0f),
			new Vector3(2f, 0f, 0f),
			new Vector3(3f, 0f, 0f),
			new Vector3(4f, 0f, 0f)
		};
		modes = new BezierControlPointMode[] {
			BezierControlPointMode.Aligned,
			BezierControlPointMode.Aligned
		};
	}

	public void AddCurve()
	{
		Vector3 point = points[points.Length - 1];
		System.Array.Resize(ref points, points.Length + 3);
		point.x += 1f;
		points[points.Length - 3] = point;
		point.x += 1f;
		points[points.Length - 2] = point;
		point.x += 1f;
		points[points.Length - 1] = point;

		System.Array.Resize(ref modes, modes.Length + 1);
		modes[modes.Length - 1] = modes[modes.Length - 2];
		EnforceMode(points.Length - 4);
    }

	public void AddCurve(Vector3 pointLast)
	{
		Vector3 point = points[points.Length - 1];
		System.Array.Resize(ref points, points.Length + 3);

		points[points.Length - 1] = pointLast;

		points[points.Length - 3] = point + (pointLast - point).normalized * controlPointHalfResolution;
		
		points[points.Length - 2] = pointLast + (point - pointLast).normalized * controlPointHalfResolution;

		System.Array.Resize(ref modes, modes.Length + 1);
		modes[modes.Length - 1] = modes[modes.Length - 2];
		EnforceMode(points.Length - 4);
	}

	public BezierControlPointMode GetControlPointMode(int index)
	{
		return modes[(index + 1) / 3];
	}

	public void SetControlPointMode(int index, BezierControlPointMode mode)
	{
		int modeIndex = (index + 1) / 3;
		modes[modeIndex] = mode;
		EnforceMode(index);
	}

	public void EnforceMode(int index)
	{
		int modeIndex = (index + 1) / 3;
		BezierControlPointMode mode = modes[modeIndex];
		if (mode == BezierControlPointMode.Free || modeIndex == 0 || modeIndex == modes.Length - 1)
		{
			return;
		}
		int middleIndex = modeIndex * 3;
		int fixedIndex, enforcedIndex;

		if (index <= middleIndex)
		{
			fixedIndex = middleIndex - 1;
			if (fixedIndex < 0)
			{
				fixedIndex = points.Length - 2;
			}
			enforcedIndex = middleIndex + 1;
			if (enforcedIndex >= points.Length)
			{
				enforcedIndex = 1;
			}
		}
		else
		{
			fixedIndex = middleIndex + 1;
			if (fixedIndex >= points.Length)
			{
				fixedIndex = 1;
			}
			enforcedIndex = middleIndex - 1;
			if (enforcedIndex < 0)
			{
				enforcedIndex = points.Length - 2;
			}
		}

		Vector3 middle = points[middleIndex];
		Vector3 enforcedTangent = middle - points[fixedIndex];
		if (mode == BezierControlPointMode.Aligned)
		{
			enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
		}
		points[enforcedIndex] = middle + enforcedTangent;
	}
}
