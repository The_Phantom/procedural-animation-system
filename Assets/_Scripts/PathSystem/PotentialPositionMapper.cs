using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PotentialPositionMapper : MonoBehaviour
{
    public class Point
    {
        public Vector3 position;
        public float distance;
        public bool possibleStepPostion;
    }   


    public Point[][] pointsMain, pointsSec;

    [SerializeField] private Transform systemRoot;
    [SerializeField] private Transform secondaryRoot;
    [SerializeField] private CentralController centralController;

    [SerializeField] private float minDistance;

    [SerializeField] private int scanResolution;//Scans per legLength

    [SerializeField] private Vector3 gravityVector;

    [SerializeField] private float limbLengthMain, limbLengthSec;

    [Header("Forward")]
    [SerializeField] private float maxForwardAngle;
    [SerializeField] private float forwardAreaAngle;


    [Header("Side")]
    [SerializeField] private float maxSideAngleHip;


    public bool mapTerrainManualScan = false;
    [SerializeField]
    private bool debugSecondary = false;

    public void Awake()
    {
        Init();
    }

    public void Update() {
        gravityVector = centralController.gravityVector;
        if(mapTerrainManualScan)
        {
            mapTerrainManualScan = false;
            MapTerrain();
        }
    }



#region Main Limbs

    private void CheckPositionMain(int _x, int _y)
    {
        
        if(!pointsMain[_x][_y].possibleStepPostion) // To far or impossible to step for other reasons already.
        {
            return;
        }
        else
        {

            //calculate local position / vector to target            

            Vector3 localPointPosition = systemRoot.InverseTransformPoint(pointsMain[_x][_y].position);
            
            //chceck if it's not too close

            if(localPointPosition.magnitude > minDistance && localPointPosition.magnitude < limbLengthMain)
            {
                //calculate main angle

                float mainAngle = Mathf.Abs( Vector3.Angle(Vector3.down, localPointPosition));
                if(mainAngle <= maxSideAngleHip) 
                {
                    return;
                }

                //calculate forward area angle

                float areaAngle = Mathf.Abs( Vector3.Angle(Vector3.forward, localPointPosition));
                float forwardAngle = Mathf.Abs( Vector3.Angle(Vector3.down, localPointPosition));
                if(forwardAngle <= maxForwardAngle && areaAngle <= forwardAreaAngle)
                {
                    return;
                }
            }
            pointsMain[_x][_y].possibleStepPostion = false;
            
        }
    }

    private void MapTerrainMain() //Terain mapper for main limbs
    {       
        Quaternion planeRotation = Quaternion.LookRotation(gravityVector, systemRoot.forward);

        float scanningStep = (limbLengthMain * 2)/scanResolution;

        Vector3 offsetedPositionPS = - new Vector3(limbLengthMain , limbLengthMain, 0);

        for(int i = 0; i < scanResolution; i++)
        {
            for(int j = 0; j < scanResolution; j++) 
            {

                

                Vector3 scanOriginPS = offsetedPositionPS + new Vector3((scanningStep*i), (scanningStep * j), 0);

                scanOriginPS = planeRotation * scanOriginPS;

                Vector3 scanOrigin = systemRoot.position + scanOriginPS;

                RaycastHit hit;
                if(Physics.Raycast(scanOrigin, gravityVector, out hit, limbLengthMain + 0.5f))
                {
                    pointsMain[i][j].distance = (scanOrigin - hit.point).magnitude;
                    pointsMain[i][j].position = hit.point;
                    pointsMain[i][j].possibleStepPostion = true;
                }
                else
                {
                    pointsMain[i][j].distance = limbLengthMain + 0.5f;
                    pointsMain[i][j].position = scanOrigin + gravityVector.normalized * (limbLengthMain + 0.5f);
                    pointsMain[i][j].possibleStepPostion = false;
                }
            }
        }
    }

#endregion


#region Secondary Limbs


    private void CheckPositionSec(int _x, int _y) // is it nessery????
    {

        if(!pointsSec[_x][_y].possibleStepPostion) // To far or impossible to step for other reasons already.
        {
            return;
        } else 
        {
        
            //calculate local position / vector to target            

            Vector3 localPointPosition = secondaryRoot.InverseTransformPoint(pointsSec[_x][_y].position);

            //chceck if it's not too close

            if(localPointPosition.magnitude > minDistance && localPointPosition.magnitude < limbLengthSec) 
            {/*
                //calculate main angle

                float mainAngle = Mathf.Abs( Vector3.Angle(Vector3.down, localPointPosition));
                if(mainAngle <= maxSideAngleHip) 
                {
                    return;
                }

                //calculate forward area angle

                float areaAngle = Mathf.Abs( Vector3.Angle(Vector3.forward, localPointPosition));
                float forwardAngle = Mathf.Abs( Vector3.Angle(Vector3.down, localPointPosition));
                if(forwardAngle <= maxForwardAngleHip && areaAngle <= forwardAreaAngle) 
                {
                    return;
                }*/
                return;            }
            pointsSec[_x][_y].possibleStepPostion = false;

        }
    }


    private void MapTerrainSec() //Terain mapper for main limbs
    {
        Quaternion planeRotation = Quaternion.LookRotation(gravityVector, secondaryRoot.forward);

        float scanningStep = (limbLengthSec * 2)/scanResolution;

        Vector3 offsetedPositionPS = - new Vector3(limbLengthSec , limbLengthSec, 0);

        for(int i = 0; i < scanResolution; i++) 
        {
            for(int j = 0; j < scanResolution; j++) 
            {



                Vector3 scanOriginPS = offsetedPositionPS + new Vector3((scanningStep*i), (scanningStep * j), 0);

                scanOriginPS = planeRotation * scanOriginPS;

                Vector3 scanOrigin = secondaryRoot.position + scanOriginPS;

                RaycastHit hit;
                if(Physics.Raycast(scanOrigin, gravityVector, out hit, limbLengthSec + 0.5f)) {
                    pointsSec[i][j].distance = (scanOrigin - hit.point).magnitude;
                    pointsSec[i][j].position = hit.point;
                    pointsSec[i][j].possibleStepPostion = true;
                } 
                else 
                {
                    pointsSec[i][j].distance = limbLengthSec + 0.5f;
                    pointsSec[i][j].position = scanOrigin + gravityVector.normalized * (limbLengthSec + 0.5f);
                    pointsSec[i][j].possibleStepPostion = false;
                }
            }
        }
    }


#endregion





    private void Init()
    {

        gravityVector = centralController.gravityVector;
        pointsMain = new Point[scanResolution][];        
        for(int i = 0; i < scanResolution; i++) 
        {
            pointsMain[i] = new Point[scanResolution];

            for(int j = 0; j < scanResolution; j++) 
            {            
                pointsMain[i][j] = new Point();
            }
        }


        pointsSec = new Point[scanResolution][];
        for(int i = 0; i < scanResolution; i++) {
            pointsSec[i] = new Point[scanResolution];

            for(int j = 0; j < scanResolution; j++) {
                pointsSec[i][j] = new Point();
            }
        }
    }


    public void MapTerrain()
    {
        MapTerrainMain();
        for(int i = 0; i < scanResolution; i++) {
            for(int j = 0; j < scanResolution; j++) {
                CheckPositionMain(i, j);
            }
        }

        MapTerrainSec();
        for(int i = 0; i < scanResolution; i++) {
            for(int j = 0; j < scanResolution; j++) {
                CheckPositionSec(i, j);
            }
        }
    }


    public Vector2Int FindClosestPointToGivenPosition(int _limbType, Vector3 _targetPosition)//returns index of point 
    {
        Vector2Int ClosestPointIndex = new Vector2Int(0, 0);

        if(_limbType == 0) //main
        {
            for(int i = 0; i < scanResolution; i++) 
            {
                for(int j = 0; j < scanResolution; j++) 
                {
                    if( (pointsMain[ClosestPointIndex.x][ClosestPointIndex.y].position-_targetPosition).sqrMagnitude > (pointsMain[i][j].position - _targetPosition).sqrMagnitude)
                    {
                        ClosestPointIndex = new Vector2Int (i,j);
                    }
                }
            }
        }
        

        return ClosestPointIndex;
    }



    private void OnDrawGizmos() 
    {
        
        
        if(!debugSecondary) //Main
        {
        
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(systemRoot.position, limbLengthMain);

            if(Application.isPlaying)
            {
                for(int i = 0; i < scanResolution; i++) 
                {
                    for(int j = 0; j < scanResolution; j++) 
                    {
                        if(pointsMain[i][j].possibleStepPostion == true)
                        {
                            Gizmos.color = Color.green;
                        } 
                        else if (pointsMain[i][j].distance > limbLengthMain)
                        {
                            Gizmos.color = Color.cyan;
                        }
                        else
                        {
                            Gizmos.color = new Color(1f, 0.75f * (pointsMain[i][j].distance / limbLengthMain), 0f, 1f);
                        }
                        Gizmos.DrawSphere(pointsMain[i][j].position, 0.05f);
                    }                            
                }
            }
        }
        else //Sec
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(secondaryRoot.position, limbLengthSec);

            if(Application.isPlaying) {
                for(int i = 0; i < scanResolution; i++) {
                    for(int j = 0; j < scanResolution; j++) {
                        if(pointsSec[i][j].possibleStepPostion == true) {
                            Gizmos.color = Color.green;
                        } else if(pointsSec[i][j].distance > limbLengthSec) {
                            Gizmos.color = Color.cyan;
                        } else {
                            Gizmos.color = new Color(1f, 0.75f * (pointsSec[i][j].distance / limbLengthSec), 0f, 1f);
                        }
                        Gizmos.DrawSphere(pointsSec[i][j].position, 0.05f);
                    }


                }
            }

        }

    }
}
