using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AStar {
    public static List<Vector2Int> FindPath(PotentialPositionMapper.Point[][] map, Vector2Int start, Vector2Int end, int giveUpLimit) {
        List<Vector2Int> path = new List<Vector2Int>();
        List<Node> open = new List<Node>();
        List<Node> closed = new List<Node>();
        bool pathNotFound = true;
        open.Add(new Node(start, 0, HeuristicFunction(end, start), null));
        Node current = null;
        int currentIter = 0;
        while(pathNotFound && open.Count > 0 && currentIter < giveUpLimit) {
            currentIter++;
            current = FindLowest(open);
            open.Remove(current);
            closed.Add(current);
            if(current.point == end) {
                pathNotFound = false;
                FillPath(path, current);
                return path;
            }
            for(int x = -1; x <= 1; x++) {
                for(int y = -1; y <= 1; y++) {
                    Vector2Int point = current.point + new Vector2Int(x, y);
                    if((x != 0 || y != 0) && Mathf.Abs(x) != Mathf.Abs(y) && (point.x >= 0 && point.x < map.Length) && (point.y >= 0 && point.y < map[0].Length)) {
                        if(!closed.Exists((n) => n.point == point)) {
                            float tmpF = current.distanceCost + map[point.x][point.y].distance;
                            Node neighbour = open.Find((n) => n.point == point);
                            if(neighbour == null) {
                                open.Add(new Node(point,
                            current.distanceCost + map[point.x][point.y].distance,
                            HeuristicFunction(end, point),
                            current));
                            } else if(neighbour.Cost < tmpF) {
                                neighbour.distanceCost = current.distanceCost + map[neighbour.point.x][neighbour.point.y].distance;
                                neighbour.heuristicCost = HeuristicFunction(end, point);
                                neighbour.preNode = current;
                            }
                        }

                    }
                }
            }
        }
        FillPath(path, current);
        Debug.LogWarning("Path not found!!!");
        return path;
    }


    //----------------------------------------SupportiveMethods-------------------------------------

    private static float HeuristicFunction(Vector2Int point1, Vector2Int point2) {
        return (point2 - point1).magnitude;
    }

    private static Node FindLowest(List<Node> list) {
        Node result = list[0];
        foreach(Node n in list) {
            if(result.Cost > n.Cost) {
                result = n;
            } else if(result.Cost == n.Cost && result.heuristicCost > n.heuristicCost) {
                result = n;
            }
        }
        return result;
    }

    private static void FillPath(List<Vector2Int> path, Node current) {
        path.Add(current.point);
        while(current.preNode != null) {
            current = current.preNode;
            path.Add(current.point);
        }
        path.Reverse();
    }
}

public class Node {
    public Vector2Int point;
    public float distanceCost, heuristicCost;
    public Node preNode;

    public float Cost {
        get {
            return distanceCost + heuristicCost;
        }
    }

    public Node(Vector2Int _point, float _distanceCost, float _heuristicCost, Node _preNode) {
        point = _point;
        distanceCost = _distanceCost;
        heuristicCost = _heuristicCost;
        preNode = _preNode;
    }
}