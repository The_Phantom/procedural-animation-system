using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointData : MonoBehaviour
{



    private void OnDrawGizmos() {

        Gizmos.color = Color.blue;

        foreach(Transform child in transform)
        {
            if(child.GetComponent<JointData>() != null)
            {
                Gizmos.DrawLine(this.transform.position, child.position);

                Gizmos.DrawSphere(this.transform.position, 0.02f);
            }

        }
    }

}
