using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class SmartIK : MonoBehaviour {

    [System.Serializable]
    public class Limb {
        public List<JointData> joints;
        public Transform limbRoot;
        public Transform target, handle;
        public int boneCount;
        public float limbLength;
        public float[] bonesLength;
        public Vector3[] startingBonesDirection;
        public Quaternion[] startingBonesRotation;
        public Quaternion StartingRotationTarget;

    };

    public List<Limb> limbs;

    [SerializeField]
    private float accuracyFactor;

    [SerializeField]
    private int maxNrOfIterations;

    [SerializeField]
    private float snappingStrength; //How much does it snap to starting position

    public bool systemActive = true;

    public void Start() {
        Init();
    }

    public void Update() {
        if(systemActive) {
            foreach(Limb limb in limbs) {
                ResolveLimbIK(limb);
            }
        }

    }

    private void ResolveLimbIK(Limb _limb) {
        if(_limb.limbRoot == null) {
            InitLimb(_limb);
        }
        if(_limb.target == null || _limb.handle == null) {
            Debug.LogError("No Target or Handle on limb! \n Limb root: " + _limb.limbRoot);
            return;
        }

        Vector3[] bonesPositionsRS = new Vector3[_limb.boneCount];
        GetBonesPositionsInRootSpace(_limb, bonesPositionsRS);

        Vector3 targetPositionRS = GetPositionInRootSpace(_limb.target, _limb);
        Quaternion targetRotationRS = GetRotationInRootSpace(_limb.target, _limb);

        if(true)//CheckForLimbReach(_limb, _limb.target.position)) // in Range
        {
            for(int i = 1; i < bonesPositionsRS.Length; i++) {
                bonesPositionsRS[i] = Vector3.Lerp(bonesPositionsRS[i], bonesPositionsRS[i - 1] + _limb.startingBonesDirection[i - 1], snappingStrength);
            }

            for(int i = 0; i < maxNrOfIterations; i++) {
                BackwardPass(_limb, bonesPositionsRS, targetPositionRS);
                ForwardPass(_limb, bonesPositionsRS);
                if(CheckAccuracy(_limb, bonesPositionsRS, targetPositionRS)) {
                    break;
                }
            }
        } else {
            Vector3 targetDirectionRS = (targetPositionRS - bonesPositionsRS[0]).normalized;
            for(int i = 1; i < bonesPositionsRS.Length; i++) {
                bonesPositionsRS[i] = bonesPositionsRS[i - 1] + targetDirectionRS * _limb.bonesLength[i - 1];
            }
        }

        //move towards pole
        if(_limb.handle != null) {
            Vector3 polePosition = GetPositionInRootSpace(_limb.handle, _limb);
            for(int i = 1; i < bonesPositionsRS.Length - 1; i++) {
                Plane plane = new Plane(bonesPositionsRS[i + 1] - bonesPositionsRS[i - 1], bonesPositionsRS[i - 1]);
                Vector3 projectedPole = plane.ClosestPointOnPlane(polePosition);
                Vector3 projectedBone = plane.ClosestPointOnPlane(bonesPositionsRS[i]);
                float angle = Vector3.SignedAngle(projectedBone - bonesPositionsRS[i - 1], projectedPole - bonesPositionsRS[i - 1], plane.normal);
                bonesPositionsRS[i] = Quaternion.AngleAxis(angle, plane.normal) * (bonesPositionsRS[i] - bonesPositionsRS[i - 1]) + bonesPositionsRS[i - 1];
            }
        }

        SetData(_limb, bonesPositionsRS, targetRotationRS);

    }

    private bool CheckForLimbReach(Limb _limb, Vector3 _targetPosition) // Not working
    {
        if((_targetPosition - _limb.joints[0].transform.position).sqrMagnitude < (_limb.limbLength * _limb.limbLength) + 0.001f) {
            return true;
        } else {
            return false;
        }

    }

    private void ForwardPass(Limb _limb, Vector3[] _bonesPositionsRS) {

        for(int i = 1; i < _bonesPositionsRS.Length; i++) {
            _bonesPositionsRS[i] = _bonesPositionsRS[i - 1] + (_bonesPositionsRS[i] - _bonesPositionsRS[i - 1]).normalized * _limb.bonesLength[i - 1];
        }
    }

    private void BackwardPass(Limb _limb, Vector3[] _bonesPositionsRS, Vector3 _targetPositionRS) {
        for(int i = _bonesPositionsRS.Length - 1; i > 0; i--) {
            if(i == _bonesPositionsRS.Length - 1) {
                _bonesPositionsRS[i] = _targetPositionRS; //tip to target
            } else {
                _bonesPositionsRS[i] = _bonesPositionsRS[i + 1] + (_bonesPositionsRS[i] - _bonesPositionsRS[i + 1]).normalized * _limb.bonesLength[i];
            }

        }
    }

    private bool CheckAccuracy(Limb _limb, Vector3[] _bonesPositionsRS, Vector3 _targetPositionRS) {
        if((_bonesPositionsRS[_bonesPositionsRS.Length - 1] - _targetPositionRS).sqrMagnitude < accuracyFactor * accuracyFactor) {
            return true;
        } else {
            return false;
        }
    }

    private void SetData(Limb _limb, Vector3[] _bonesPositionsRS, Quaternion _targetRotationRS) {
        for(int i = 0; i < _bonesPositionsRS.Length; i++) {
            if(i == _bonesPositionsRS.Length - 1) {
                SetRotationInRootSpace(_limb.joints[i].transform, Quaternion.Inverse(_targetRotationRS) * _limb.StartingRotationTarget * Quaternion.Inverse(_limb.startingBonesRotation[i]), _limb);

            } else {
                SetRotationInRootSpace(_limb.joints[i].transform, Quaternion.FromToRotation(_limb.startingBonesDirection[i], _bonesPositionsRS[i + 1] - _bonesPositionsRS[i]) * Quaternion.Inverse(_limb.startingBonesRotation[i]), _limb);
                SetPositionInRootSpace(_limb.joints[i].transform, _bonesPositionsRS[i], _limb);
            }
        }
    }


    private void Init() {
        for(int i = 0; i < limbs.Count; i++) {
            InitLimb(limbs[i]);
        }
    }

    private void InitLimb(Limb _limb) {
        _limb.boneCount = _limb.joints.Count;
        _limb.StartingRotationTarget = GetRotationInRootSpace(_limb.target, _limb);
        _limb.bonesLength = new float[_limb.boneCount-1];
        _limb.startingBonesDirection = new Vector3[_limb.joints.Count];
        _limb.startingBonesRotation = new Quaternion[_limb.joints.Count];
        SetBonesData(_limb);
    }

    private void SetBonesData(Limb _limb) {
        _limb.limbLength = 0;

        for(int i = _limb.joints.Count - 1; i >= 0; i--) {
            _limb.startingBonesRotation[i] = GetRotationInRootSpace(_limb.joints[i].transform, _limb);

            if(i == _limb.joints.Count - 1)//tip
            {
                _limb.startingBonesDirection[i] = GetPositionInRootSpace(_limb.target, _limb) - GetPositionInRootSpace(_limb.joints[i].transform, _limb);
            } else //mid
              {
                _limb.startingBonesDirection[i] = GetPositionInRootSpace(_limb.joints[i + 1].transform, _limb) - GetPositionInRootSpace(_limb.joints[i].transform, _limb);
                _limb.bonesLength[i] = _limb.startingBonesDirection[i].magnitude;
                _limb.limbLength += _limb.startingBonesDirection[i].magnitude;
            }

        }
    }
    ////////////////////////
    ///

    #region Support Function

    private Vector3 GetPositionInRootSpace(Transform _object, Limb _limb) {
        if(_limb.limbRoot == null) {
            return _object.position;
        } else {
            return Quaternion.Inverse(_limb.limbRoot.rotation) * (_object.position - _limb.limbRoot.position);
        }

    }

    private void SetPositionInRootSpace(Transform _object, Vector3 _value, Limb _limb) {
        if(_limb.limbRoot == null) {
            _object.position = _value;
        } else {
            _object.position = _limb.limbRoot.rotation * _value + _limb.limbRoot.position;
        }
    }

    private Quaternion GetRotationInRootSpace(Transform _object, Limb _limb) {
        if(_limb.limbRoot == null) {
            return _object.rotation;
        } else {
            return Quaternion.Inverse(_object.rotation) * _limb.limbRoot.rotation;
        }
    }

    private void SetRotationInRootSpace(Transform _object, Quaternion _value, Limb _limb) {
        if(_limb.limbRoot == null) {
            _object.rotation = _value;
        } else {
            _object.rotation = _limb.limbRoot.rotation * _value;
        }
    }

    private void GetBonesPositionsInRootSpace(Limb _limb, Vector3[] _positionsRS) {
        for(int i = 0; i < _limb.boneCount; i++) {
            _positionsRS[i] = GetPositionInRootSpace(_limb.joints[i].transform, _limb);
        }
    }




    #endregion



#if UNITY_EDITOR
void OnDrawGizmos() {

    for(int i = 0; i < limbs.Count; i++) {
        for(int j = limbs[i].boneCount - 2; j >= 0; j--) {
            float scale = Vector3.Distance(limbs[i].joints[j].transform.position, limbs[i].joints[j+1].transform.position)*0.1f;
            Handles.matrix = Matrix4x4.TRS(limbs[i].joints[j].transform.position, Quaternion.FromToRotation(Vector3.up, limbs[i].joints[j + 1].transform.position - limbs[i].joints[j].transform.position), new Vector3(scale, Vector3.Distance(limbs[i].joints[j + 1].transform.position, limbs[i].joints[j].transform.position), scale));
            Handles.color = Color.green;
            Handles.DrawWireCube(Vector3.up * 0.5f, Vector3.one);
        }
    }

    /*
    var current = this.transform;
    for(int i = 0; i < ChainLength && current != null && current.parent != null; i++) {
        var scale = Vector3.Distance(current.position, current.parent.position) * 0.1f;
        Handles.matrix = Matrix4x4.TRS(current.position, Quaternion.FromToRotation(Vector3.up, current.parent.position - current.position), new Vector3(scale, Vector3.Distance(current.parent.position, current.position), scale));
        Handles.color = Color.green;
        Handles.DrawWireCube(Vector3.up * 0.5f, Vector3.one);
        current = current.parent;
    }
    /*if(Root != null)
    {
        Handles.matrix = Matrix4x4.TRS(Root.position, Quaternion.identity, new Vector3(0.1f, 0.1f,0.1f));
        Handles.color = Color.red;
        Handles.DrawWireCube(Vector3.up * 0.75f, Vector3.one);
    }*/

}
#endif
}
