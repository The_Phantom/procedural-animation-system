using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimbController : MonoBehaviour
{
    public int limbId;
    public int limbType;//0-Main, 1-Secondary
    public int limbStatus; //0-static, 1-moving, -1-overreach
    public float pathProgress;
    public float limbPathTravelTime;
    public float limbStepHeight;

    [SerializeField]
    private BezierSpline bezierSpline;

    [SerializeField]
    private SmartIK smartIK;

    [SerializeField]
    private CentralController centralController;

    public void Update() {
        
        if(limbStatus == 1)
        {
            MoveLimbAlongPath();
        }
        else if(limbStatus == 0)
        {
            CheckForOverreach();
            AlignFoot();
        }        
    }

    private void CheckForOverreach()
    {
        float distance = (smartIK.limbs[limbId].joints[smartIK.limbs[limbId].joints.Count-1].transform.position - smartIK.limbs[limbId].joints[0].transform.position).magnitude;
        
        if(distance >= smartIK.limbs[limbId].limbLength - 0.01)
        {
            limbStatus = -1;
        }
    }

    public void StartMovment(List<Vector3> _path)
    {
        pathProgress = 0;

        CreateBezierPath(_path);

        limbStatus = 1;
    }
    private void MoveLimbAlongPath()
    {
        pathProgress = Mathf.Clamp01(pathProgress + Time.deltaTime/limbPathTravelTime);
        
        smartIK.limbs[limbId].target.position = bezierSpline.GetPoint(pathProgress);

        if(pathProgress == 1) //Movment Completed
        {
            limbStatus = 0;

            centralController.systemStatus = 1; // later to some function that checks that

            smartIK.limbs[limbId].target.position = bezierSpline.GetPoint(1f);

            pathProgress = 0;
        }
    }


    private void AlignFoot()
    {
        Vector3 scanOrigin = smartIK.limbs[limbId].target.position + (-centralController.gravityVector*0.15f);

        RaycastHit hit;
        if(Physics.Raycast(scanOrigin, centralController.gravityVector, out hit, 0.75f)) {

            smartIK.limbs[limbId].target.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

        }
    }

    public void CreateBezierPath(List<Vector3> _path)
    {
        CurveCreator.CreateCurve(bezierSpline, _path, BezierControlPointMode.Aligned, 0.084f);
    }

}
