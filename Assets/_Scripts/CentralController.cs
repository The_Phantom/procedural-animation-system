using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralController : MonoBehaviour 
{


    public int systemStatus; //0-Out of Balance, 1-Stable, 2-Restoring Balance

    [SerializeField]
    private Transform root; //Used as center of mass.



    //Limbs
    [SerializeField]
    private List<Vector3> mainLimbsBaseOffset;

    [SerializeField]
    private List<int> balancingLimbs; //Limbs that are used to balance body. First part of list are mainLimbs, second are secondary.


    [SerializeField]
    private SmartIK smartIK;

    [SerializeField]
    private List<LimbController> limbControllers;

    [SerializeField]
    private float targetOffsetFromGround;

    [SerializeField]
    private float targetOvertake;

    [SerializeField]
    private List<Vector3> balancePoints;

    public Vector3 gravityVector;

    public Vector3 MotionVector;

    [SerializeField]
    private float balanceExtentRadius;

    [SerializeField]
    private PotentialPositionMapper mapper;


    [SerializeField]
    private float minHeightBodyOffset, maxHeightBodyOffset;



    private Vector3 lastSystemPosition;

    private float lastBodyVerticalOffset;


    //Move command

    //Secondary limb Controll

    public void Awake() 
    {
        Init();
    }


    public void Update() 
    {

        CalculateMotionVector();

        CheckForOverreachedLimbs();

        if(systemStatus == 1)
        {
    
            if(CheckIfCenterOfMassIsInBalanceArea())
            {
                systemStatus = 1;
            }
            else
            {
                systemStatus = 0;
            }
        }
        if(systemStatus == 0)
        {
            MoveLimbToRebalance();
        }
        MoveAllSecondaryLimbs();

        BodyMovment();
    }

    #region Balance System
    private bool CheckIfCenterOfMassIsInBalanceArea()
    {

        if(balancePoints.Count <= 0) {
            throw new System.Exception("Central Controller does not have any balance points available!");
        }

        Vector3 centerOfMassGAS = Quaternion.Inverse(Quaternion.FromToRotation(Vector3.forward, gravityVector))*root.position;

        List<Vector3> balancePointsGAS = new List<Vector3>(balancePoints.Count);

        for (int i = 0; i < balancePoints.Count; i++) //Calc balancePoints into Gravity Aligned Space
        {
            balancePointsGAS.Add(Quaternion.Inverse(Quaternion.FromToRotation(Vector3.forward, gravityVector))*balancePoints[i]);
            Vector3 tmp = balancePointsGAS[i];
            tmp.z = 0;
            balancePointsGAS[i] = tmp;
        }

        CalculateBoundry(ref balancePointsGAS);

        foreach(Vector3 point in balancePointsGAS)
        {
            if((centerOfMassGAS - point).magnitude < balanceExtentRadius) {
                return true;
            }
        }
        if(balancePointsGAS.Count == 1) 
        {

        }
        if(balancePointsGAS.Count >= 3) //Check if center of mass is inside convex;
        {
            for(int i = 0;  i < balancePointsGAS.Count - 2; i++)
            {
                if(CheckIfPointIsInTriangle(centerOfMassGAS, balancePointsGAS[0], balancePointsGAS[i+1], balancePointsGAS[i+2])){
                    return true;
                }
            }
        }
        if(balancePointsGAS.Count >= 2) //Check if center of mass is near edge;
        {
            for(int i = 0; i < balancePointsGAS.Count - 1; i++) 
            {
                
                if(CheckRangeOfEdge(new List <Vector3>(){balancePointsGAS[i], balancePointsGAS[i+1]}, centerOfMassGAS, balanceExtentRadius))
                {
                    return true;
                }

            }
            if(CheckRangeOfEdge(new List<Vector3>() { balancePointsGAS[0], balancePointsGAS[balancePointsGAS.Count-1] }, centerOfMassGAS, balanceExtentRadius)) {
                return true;
            }
        }

        return false;

    }

    private bool CheckRangeOfEdge(List<Vector3> _edge, Vector3 _point, float _range)
    {
        Vector2 transformedPoint = CastVectorValues(_point - _edge[0], _edge[1] - _edge[0]);
        if(transformedPoint.x >= 0f && transformedPoint.x <= (_edge[1] - _edge[0]).magnitude && Mathf.Abs(transformedPoint.y) <= _range)
        {
            return true;
        }
        return Mathf.Min((_edge[0] - _point).sqrMagnitude, (_edge[1] - _point).sqrMagnitude) <= _range;


    }

    private bool CheckDelta(float _delta, float _a, float _b)
    {
        if(_delta <0)
        {
            return false;
        }
        
        float t1, t2;

        t1 = (-_b - Mathf.Sqrt(_delta)) / (2 * _a);
        t2 = (-_b + Mathf.Sqrt(_delta)) / (2 * _a);
        
        if(t1>t2)
        {
            float tmp = t1;
            t1 = t2;
            t2 = tmp;
        }
        if (t1 < 0 && t2 < 0)
        {
            return false;
        }
        if(t1 > 1 && t2 > 1) 
        {
            return false;
        }
        return true;

    }

    private void CalculateBoundry(ref List<Vector3> _balancePointsGAS)
    {
        if(_balancePointsGAS.Count <=3)
        {
            return;
        }

        //Find points with minimal and maximum X value.

        int minXPointIndex = 0, maxXPointIndex = 0;

        

        for(int i = 1; i < _balancePointsGAS.Count; i++)
        {
            if(_balancePointsGAS[i].x < _balancePointsGAS[minXPointIndex].x)
            {
                minXPointIndex = i;
            }
            if(_balancePointsGAS[i].x == _balancePointsGAS[minXPointIndex].x) 
            {
                if(_balancePointsGAS[i].y < _balancePointsGAS[minXPointIndex].y) 
                {
                    minXPointIndex = i;
                }
            }
            if(_balancePointsGAS[i].x > _balancePointsGAS[maxXPointIndex].x) {
                maxXPointIndex = i;
            }
            if(_balancePointsGAS[i].x == _balancePointsGAS[maxXPointIndex].x) 
            {
                if(_balancePointsGAS[i].y > _balancePointsGAS[maxXPointIndex].y) 
                {
                    maxXPointIndex = i;
                }
            }
        }

        List<Vector3> baseLine = new List<Vector3>(){_balancePointsGAS[minXPointIndex], _balancePointsGAS[maxXPointIndex]};
        List<Vector3> subsetOne = new List<Vector3>();
        List<Vector3> subsetTwo = new List<Vector3>();

        CreateSubsets(_balancePointsGAS, baseLine, subsetOne, subsetTwo);

        List<Vector3> convexList = new List<Vector3>(){baseLine[0]};

        foreach(Vector3 position in DivideSet(subsetOne, baseLine))
        {
            convexList.Add(position);
        }

        convexList.Add(baseLine[1]);
        baseLine.Reverse();
        foreach(Vector3 position in DivideSet(subsetTwo, baseLine)) 
        {
            convexList.Add(position);
        }

        _balancePointsGAS = convexList;
    }

    private void CreateSubsets(List<Vector3> _mainSet, List<Vector3> _baseLine, List<Vector3> _subsetOne, List<Vector3> _subsetTwo)
    {
        for (int i = 0; i < _mainSet.Count; i++)
        {
            if(_mainSet[i] == _baseLine[0] || _mainSet[i] == _baseLine[1]){
                continue;
            }
            if(CastVectorValues(_baseLine[1] - _baseLine[0], _mainSet[i] - _baseLine[0]).y>0){
                _subsetOne.Add(_mainSet[i]);
            }else{
                _subsetTwo.Add(_mainSet[i]);
            }
        }
    }

    private List<Vector3> DivideSet(List<Vector3> _mainSet, List<Vector3> _baseLine)
    {
        if(_mainSet.Count < 2)
        {
            return new List<Vector3>(_mainSet);
        }

        int maxDistancePointIndex = 0;
        float maxDistance=0;
        for(int i = 0; i < _mainSet.Count; i++) 
        {
            float tmpDistance = DistanceFromLine(_baseLine, _mainSet[i]);
            if(tmpDistance > maxDistance)
            {
                maxDistance = tmpDistance;
                maxDistancePointIndex = i;
            }
        }

        Vector3 maxDistancePoint = _mainSet[maxDistancePointIndex];

        _mainSet.RemoveAll((v3)=>CheckIfPointIsInTriangle(v3, _baseLine[0], _baseLine[1], maxDistancePoint));

        List<Vector3> subsetOne = new List<Vector3>();
        List<Vector3> subsetTwo = new List<Vector3>();

        CreateSubsets(_mainSet, new List<Vector3>(){ _baseLine[0], maxDistancePoint}, subsetOne, subsetTwo);

        List<Vector3> resultSet = new List<Vector3>();

        foreach(Vector3 position in DivideSet(subsetOne, new List<Vector3>() { _baseLine[0], maxDistancePoint })) {
            resultSet.Add(position);
        }
        resultSet.Add(maxDistancePoint);

        subsetOne.Clear();
        subsetTwo.Clear();

        CreateSubsets(_mainSet, new List<Vector3>() { maxDistancePoint, _baseLine[1] }, subsetOne, subsetTwo);

        foreach(Vector3 position in DivideSet(subsetOne, new List<Vector3>() { maxDistancePoint, _baseLine[1] })) {
            resultSet.Add(position);
        }

        return resultSet;
    }


    private Vector2 CastVectorValues(Vector2 _vectorToCast, Vector2 _axisToCastOn) 
    {
        _axisToCastOn.Normalize();
        Vector2 result = new Vector2(
            x: (_axisToCastOn.x*_vectorToCast.x) + (_axisToCastOn.y*_vectorToCast.y),
            y: -(_vectorToCast.x*_axisToCastOn.y) + ( _axisToCastOn.x*_vectorToCast.y)
        );
        return result;
    }

    private float DistanceFromLine(List<Vector3> _baseLine, Vector3 _point)
    {
        return Mathf.Abs(CastVectorValues(_baseLine[1] - _baseLine[0], _point - _baseLine[0]).y);
    }

    private bool CheckIfPointIsInTriangle(Vector3 _point, Vector3 vertex0, Vector3 vertex1, Vector3 vertex2)
    {
        int trueCounter = 0;
        Vector3 dir0 = vertex0 - _point;
        Vector3 dir1 = vertex1 - _point;
        Vector3 dir2 = vertex2 - _point;

        if(dir0.x * dir1.y - dir0.y * dir1.x > 0) {
            trueCounter++;
        }
        if(dir1.x * dir2.y - dir1.y * dir2.x > 0) {
            trueCounter++;
        }
        if(dir2.x * dir0.y - dir2.y * dir0.x > 0) {
            trueCounter++;
        }

        if(trueCounter == 3 || trueCounter == 0) {
            return true;
        }

        return false;
    }

    #endregion

    #region Move Limb System


    private void MoveLimbToRebalance()
    {
        mapper.MapTerrain();

        LimbController limbToMove = null;
        float maxDistanceToTarget = 0;

        Vector2Int closestEndPositionIndex = new Vector2Int(0,0);
        Vector3 closestEndPositionToBest = new Vector3();

        Vector2Int choosenClosestEndPositionIndex = new Vector2Int();
        Vector3 choosenClosestEndPositionToBest = new Vector3();

        foreach(LimbController limbController in limbControllers) {



            Vector3 possibleBestEndPosition = CalculateBestLimbPosition(limbController.limbId);
            closestEndPositionIndex = mapper.FindClosestPointToGivenPosition(limbController.limbType, possibleBestEndPosition); //fix to chcek only in possible
            closestEndPositionToBest = limbController.limbType == 0 ? mapper.pointsMain[closestEndPositionIndex.x][closestEndPositionIndex.y].position : mapper.pointsSec[closestEndPositionIndex.x][closestEndPositionIndex.y].position;
            if(CheckBalanceWithSuggested(closestEndPositionToBest, limbController.limbId))
            {
                float tmpDistance = (closestEndPositionToBest - smartIK.limbs[limbController.limbId].target.position).magnitude;
                if(tmpDistance > maxDistanceToTarget)
                {
                    choosenClosestEndPositionIndex = closestEndPositionIndex;
                    choosenClosestEndPositionToBest = closestEndPositionToBest;
                    maxDistanceToTarget = tmpDistance;
                    limbToMove = limbController;
                }

            }
            closestEndPositionIndex = choosenClosestEndPositionIndex;
            closestEndPositionToBest = choosenClosestEndPositionToBest;
        }
        if(limbToMove == null)
        {
            MoveOutOfBalance();
            return;
        }
        else
        {
            if(CheckIfLimbIsBalancing(limbToMove))
            {
                //TODO: Rework
            }
            balancePoints[limbToMove.limbId] = closestEndPositionToBest;   
        }

        systemStatus = 2;

        Vector2Int startPositionIndex = mapper.FindClosestPointToGivenPosition(limbToMove.limbType, smartIK.limbs[limbToMove.limbId].target.position);

        List<Vector2Int> pathPointsIndexes;
        List<Vector3> translatedPath = new List<Vector3>();

        if(limbToMove.limbType == 0)
        {

            pathPointsIndexes = AStar.FindPath(mapper.pointsMain, startPositionIndex, closestEndPositionIndex, 500);
            translatedPath = TranslatePath(pathPointsIndexes, mapper.pointsMain, limbToMove);


        }
        else
        {
            pathPointsIndexes = AStar.FindPath(mapper.pointsSec, startPositionIndex, closestEndPositionIndex, 500);
            translatedPath = TranslatePath(pathPointsIndexes, mapper.pointsSec, limbToMove);
        }
        translatedPath[0] = smartIK.limbs[limbToMove.limbId].target.position;
        limbToMove.StartMovment(translatedPath);

    }

    private void MoveLimbOverreached(LimbController _limbController)
    {
        mapper.MapTerrain();


        Vector2Int closestEndPositionIndex = new Vector2Int(0,0);
        Vector3 closestEndPositionToBest = new Vector3();
        Vector3 possibleBestEndPosition = CalculateBestLimbPosition(_limbController.limbId);
            closestEndPositionIndex = mapper.FindClosestPointToGivenPosition(_limbController.limbType, possibleBestEndPosition);
            closestEndPositionToBest = _limbController.limbType == 0 ? mapper.pointsMain[closestEndPositionIndex.x][closestEndPositionIndex.y].position : mapper.pointsSec[closestEndPositionIndex.x][closestEndPositionIndex.y].position;
            if(CheckBalanceWithSuggested(closestEndPositionToBest, _limbController.limbId)) 
            {

            }

        
            balancePoints[_limbController.limbId] = closestEndPositionToBest;


        //systemStatus = 2;

        Vector2Int startPositionIndex = mapper.FindClosestPointToGivenPosition(_limbController.limbType, smartIK.limbs[_limbController.limbId].target.position);

        List<Vector2Int> pathPointsIndexes;
        List<Vector3> translatedPath = new List<Vector3>();

        if(_limbController.limbType == 0) {

            pathPointsIndexes = AStar.FindPath(mapper.pointsMain, startPositionIndex, closestEndPositionIndex, 500);
            translatedPath = TranslatePath(pathPointsIndexes, mapper.pointsMain, _limbController);


        } else {
            pathPointsIndexes = AStar.FindPath(mapper.pointsSec, startPositionIndex, closestEndPositionIndex, 500);
            translatedPath = TranslatePath(pathPointsIndexes, mapper.pointsSec, _limbController);
        }

        _limbController.StartMovment(translatedPath);
    }

    private void MoveAllSecondaryLimbs()
    {
        foreach(LimbController limbController in limbControllers) 
        {
            if(!CheckIfLimbIsBalancing(limbController))
            {
                SecondaryLimbNormalMovment(limbController);
            }
        }
    }

    private void SecondaryLimbNormalMovment(LimbController _limbController)
    {
        //TODO: Implement Follow
    }
    
    private void MoveOutOfBalance()
    {
        foreach(LimbController limbController in limbControllers) 
        {
            if(limbController.limbType != 0) 
            {

            }
        }
    }

    private void BodyMovment()
    {
        float yOffsetSum = minHeightBodyOffset;
        
        foreach(LimbController limbController in limbControllers) 
        {
            if(limbController.limbType == 0) 
            {
                yOffsetSum += 4*limbController.pathProgress*(1- limbController.pathProgress)*(maxHeightBodyOffset);
            }
        }


        root.position = root.position + (yOffsetSum-lastBodyVerticalOffset) * root.up;

        lastBodyVerticalOffset = yOffsetSum;

    }

    private bool CheckIfLimbIsBalancing(LimbController _limbController)
    {
        return balancingLimbs.Contains(_limbController.limbId);
    }


    private bool CheckBalanceWithSuggested(Vector3 _newPosition, int _limbId)
    {
        List<Vector3> oldBalancePoints = new List<Vector3>(balancePoints);
        int limbIdBalanceLists = balancingLimbs.FindIndex((n)=>n==_limbId);

        balancePoints[limbIdBalanceLists] = _newPosition;

        if(CheckIfCenterOfMassIsInBalanceArea()) 
        {
            balancePoints = new List<Vector3>(oldBalancePoints);
            return true;
        } else 
        {
            balancePoints = new List<Vector3>(oldBalancePoints);
            return false;
        }
    }

    private Vector3 CalculateBestLimbPosition(int _limbId)
    {
        Vector3 calculationRefrencePoint = root.position;
        if(limbControllers[_limbId].limbType == 0)
        {
            calculationRefrencePoint = root.TransformPoint(mainLimbsBaseOffset[_limbId]);
        }
        else
        {
            calculationRefrencePoint = smartIK.limbs[_limbId].limbRoot.position;
        }

        Vector3 bestTargetLocation = calculationRefrencePoint + MotionVector / Time.deltaTime * limbControllers[_limbId].limbPathTravelTime * targetOvertake;
        return bestTargetLocation;
    }

    private List<Vector3> TranslatePath(List<Vector2Int> _pathPointsIndexes, PotentialPositionMapper.Point[][] _map, LimbController _limbController)
    {
        List <Vector3> translatedPath= new List<Vector3>();

        Vector3 tmpPosition = _map[_pathPointsIndexes[0].x][_pathPointsIndexes[0].y].position + -gravityVector * targetOffsetFromGround;

        translatedPath.Add(tmpPosition); //Start Position

        for(int i = 1; i < _pathPointsIndexes.Count -1; i++)
        {
            Vector3 tmpPos = _map[_pathPointsIndexes[i].x][_pathPointsIndexes[i].y].position;
            if(_limbController.limbType == 0)
            {
                tmpPos += -gravityVector * (_limbController.limbStepHeight + targetOffsetFromGround); //HeightOffset
            }
            translatedPath.Add(tmpPos);
        }

        tmpPosition = _map[_pathPointsIndexes[_pathPointsIndexes.Count - 1].x][_pathPointsIndexes[_pathPointsIndexes.Count - 1].y].position + -gravityVector * targetOffsetFromGround;


        translatedPath.Add(tmpPosition); //End Position



        return translatedPath;
    }


    #endregion

    private void CheckForOverreachedLimbs()
    {
        foreach(LimbController limbController in limbControllers)
        {
            if(limbController.limbStatus == -1)
            {
                if(limbController.limbType != 0) //Remove from balancing Limbs if not main
                {
                    int limbIdBalanceLists = balancingLimbs.FindIndex((n) => n == limbController.limbId);
                    balancingLimbs.RemoveAt(limbIdBalanceLists);
                    balancePoints.RemoveAt(limbIdBalanceLists);
                }

                MoveLimbOverreached(limbController);
            }
        }
    }


    private void Init()
    {
        lastSystemPosition = root.position;
        balancingLimbs.Clear();
        balancePoints.Clear();
        mainLimbsBaseOffset.Clear();
        foreach(LimbController limbController in limbControllers) 
        {

            smartIK.limbs[limbController.limbId].target.position = smartIK.limbs[limbController.limbId].joints[smartIK.limbs[limbController.limbId].joints.Count-1].transform.position;
            if(limbController.limbType == 0)
            {
                balancingLimbs.Add(limbController.limbId);
                balancePoints.Add(smartIK.limbs[limbController.limbId].target.position);
                mainLimbsBaseOffset.Add(root.InverseTransformPoint(smartIK.limbs[limbController.limbId].target.position));
            }
            
        
        }
    }

    private void CalculateMotionVector()
    {
        MotionVector = root.position - lastSystemPosition;
        lastSystemPosition = root.position;
    }






    private void OnDrawGizmos()
    {

        Gizmos.color = Color.yellow;

        foreach (Vector3 position in balancePoints)
        {
            Gizmos.DrawWireCube(position, new Vector3(0.1f,0.1f,0.1f));
        }
        if(balancePoints.Count > 0)
        {
            if(CheckIfCenterOfMassIsInBalanceArea()) {
                Gizmos.color = Color.green;
            } else {
                Gizmos.color = Color.red;
            }

            List<Vector3> tmpConvex = new List<Vector3>(balancePoints);
            for(int i = 0; i < tmpConvex.Count; i++) {
                tmpConvex[i] = new Vector3(tmpConvex[i].x, tmpConvex[i].z, 0f);
            }


            CalculateBoundry(ref tmpConvex);

            for(int i = 0; i < tmpConvex.Count; i++) {
                tmpConvex[i] = new Vector3(tmpConvex[i].x, 0f, tmpConvex[i].y);
            }

            for(int i = 1; i < tmpConvex.Count; i++) {
                Gizmos.DrawLine(tmpConvex[i - 1], tmpConvex[i]);
            }
            Gizmos.DrawLine(tmpConvex[0], tmpConvex[tmpConvex.Count - 1]);
            Gizmos.DrawSphere(root.position, 0.15f);
        }


        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(root.position + -gravityVector * 0.25f + MotionVector / Time.deltaTime * limbControllers[0].limbPathTravelTime*targetOvertake, 0.1f);

    }
}
